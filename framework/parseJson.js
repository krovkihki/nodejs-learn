module.exports = (request, response) => {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.send = (data) => {
        response.end(JSON.stringify(data));
    };
};
