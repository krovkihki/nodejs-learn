module.exports = class Router {
    // создаем объект Router
    constructor() {
        this.endpoints = {};
    }

    request(method = 'GET', path, handler) {
        if (!this.endpoints[path]) {
            this.endpoints[path] = {};
        }
        const endpoint = this.endpoints[path]; // забираем endpoint из пути

        if (endpoint[method]) {
            throw new Error(`${method} по адресу  ${path} уже существует`); // обрабатываем ошибку
        }
        endpoint[method] = handler;
    }
    get(path, handler) {
        this.request('GET', path, handler); //конкретно указываем метод GET для роуту и тд
    }
    post(path, handler) {
        this.request('POST', path, handler);
    }
    put(path, handler) {
        this.request('PUT', path, handler);
    }
    delete(path, handler) {
        this.request('DELETE', path, handler);
    }
};
console.log(__dirname);
