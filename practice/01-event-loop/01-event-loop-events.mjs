import fs from 'fs';
import dns from 'dns';

function timestamp() {
    return performance.now().toFixed(2);
}
// import inquirer from "inquirer";
// const fs = require('fs')

fs.writeFile('./test.txt', 'testetsttest', () =>
    console.log('done', performance.now())
);

console.log('start');

Promise.resolve().then(() => console.log('promise 1', performance.now()));
setTimeout(() => console.log('timeout 1', timestamp(), performance.now()), 0);
setTimeout(() => {
    process.nextTick(() => console.log('Next tick 2', timestamp()));
    console.log('timeout 2', performance.now()), 10;
});

process.nextTick(() => console.log('Next Tick 1', performance.now()));
setImmediate(() => console.log('immediate 1', timestamp()));
//вот тут
dns.lookup('google.com', (err, address, family) => {
    console.log('DNS 1 google.com', address, timestamp());
});

console.log('end');
/* start
end
promise 1 46.642199993133545
Next Tick 1 47.77649998664856
immediate 1 48.31
timeout 1 48.66 48.667399883270264
timeout 2 48.897499799728394
Next tick 2 49.26
done 49.95599985122681
DNS 1 google.com 216.58.210.174 50.57 !!!
*/
