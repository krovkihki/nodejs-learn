let isRunning = true;
setTime;
setTimeout(() => (isRunning = false), 10); // сюда мы не дойдем
process.nextTick(() => console.log('nextTick')); // как и сюда

while (isRunning) {
    console.log('while is running'); // будет выводится бесконечно в терминале
}
