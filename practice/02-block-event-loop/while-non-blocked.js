fs = require('fs');
let isRunning = true;

setTimeout(() => (isRunning = false), 10); // сюда мы не дойдем

function setImmediatePromise() {
    return new Promise((resolve, reject) => {
        process.nextTick(() => console.log('nextTick')); // как и сюда
        setImmediate(() => resolve());
    });
}

async function whileLoop() {
    while (isRunning) {
        console.log('while is running'); // будет выводится бесконечно в терминале
        await setImmediatePromise();
    }
}

whileLoop().then(() => console.log('loop ended'));
