/* Событийно ориентированная модель */
const Emitter = require('events');

const emitter = new Emitter();
emitter.on('message', (data,second,third)=>{
    console.log('Вы прислали сообщение ' + data);
    console.log('Второй аргумент ' + second);
})
// const callback = (data, second, third) => {
   
// }
const MESSAGE = process.env.message || '';

if (MESSAGE) {
    emmitter.emit('message', MESSAGE, 123)// для генерации события используется emit
} else {
    emitter.emit('message', 'Вы не указали сообщение')
}
/* Вы прислали сообщение Вы не указали сообщение
Второй аргумент undefined */

emitter.once('message', callback) // для вызова единожды

emitter.emit('message')// сколько угодно раз
emitter.emit('message')
emitter.emit('message')
emitter.emit('message')
emitter.emit('message')

emitter.removeAllListeners()// убиваем все слушатели
emitter.removeListener('message', callback)// конкретный

// Когда удобно использовать?
// http
// websockets
// long pulling
// clusters