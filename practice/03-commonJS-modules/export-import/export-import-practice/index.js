const exportedObject  = require ('./multiple-export');
const greetings = require ('./folder-to-module/single-export'); //'/Users/Виталя/Desktop/test/03-commonJS-modules/export-import/export-import-practice/single-export.js' - абсолютный путь
const {myName:myOtherName, myFriendsName,myGreatHobbies} = require('./export-and-import');// переименовываем во избежание ошибок


console.log(exportedObject);
console.log(exportedObject.myName); //MyName свойство объекта exports
console.log(exportedObject.myHobbies);
console.log(exportedObject.myFavoriteNumber);



const {myName, myHobbies, myFavoriteNumber} = exportedObject;// деструктурируем свойство объекта exports

console.log(myName); //MyName свойство объекта exports
console.log(myHobbies);
console.log(myFavoriteNumber);

myHobbies.push('Cooking');
console.log(myHobbies); //[ 'Sports', 'Music', 'Cooking', 'Cooking' ]
console.log(myGreatHobbies);// это объект, а массивы ссылаются на один и тот же объект
/* text from multi-export module!!
{
  myName: 'John Doe',
  myHobbies: [ 'Sports', 'Music', 'Cooking' ],
  myFavoriteNumber: 42
}
John Doe
[ 'Sports', 'Music', 'Cooking' ]
42
John Doe
[ 'Sports', 'Music', 'Cooking' ]
42 */

greetings(myName); //Hello  John Doe

console.log(myOtherName);
console.log(myFriendsName);