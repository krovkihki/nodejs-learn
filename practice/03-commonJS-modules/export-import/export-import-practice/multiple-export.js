const myName = 'John Doe';
const myHobbies = ['Sports', 'Music', 'Cooking'];
const myFavoriteNumber = 42;

console.log('text from multi-export module');

module.exports.myName = myName;
exports.myHobbies = myHobbies;
exports.myFavoriteNumber = myFavoriteNumber; 
//text from multi-export module!!