const http = require('http'); // import http module

const PORT = process.env.PORT || 5000; // set the port из ENV или 5000

const users = {
    name: 'test',
    email: 'kihki',
    age: 'mnoga'
}
const server = http.createServer((request, response) => {
    response.writeHead(200, { 'Content-Type': 'application/json' }); // set the content type and charset
    if(request.url === '/users'){
        return response.end(JSON.stringify(users))
    }if(request.url === '/products'){
        return  response.end("products")
    }else 
    return response.end('takogo url net') 
});

server.listen(PORT, () => console.log(`Server is running on port ${PORT}`)); // если сервер запущен, то выводим сообщение
