const path = require('path');
console.log(path.join('one', 'two', 'three')); // one/two/three
// console.log(path.join(__dirname, 'one', 'two', 'three')); // c:\Users\Виталя\Desktop\test\one\two\three
console.log(path.resolve()); //C:\Users\Виталя\Desktop\test
const pathToDir = path.resolve(); 
console.log(path.parse(pathToDir))
/* {
  root: 'C:\\',
  dir: 'C:\\Users\\Виталя\\Desktop',
  base: 'test',
  ext: '',
  name: 'test'
} */
const siteURL = 'https://gis-api.admlr.lipetsk.ru/api/v1/roads/traffic_lights/points';
const url = new URL(siteURL);
console.log(url)
/* URL {
URL {
  href: 'https://gis-api.admlr.lipetsk.ru/api/v1/roads/traffic_lights/points',      
  origin: 'https://gis-api.admlr.lipetsk.ru',
  protocol: 'https:',
  username: '',
  password: '',
  host: 'gis-api.admlr.lipetsk.ru',
  hostname: 'gis-api.admlr.lipetsk.ru',
  port: '',
  pathname: '/api/v1/roads/traffic_lights/points',
  search: '',
  searchParams: URLSearchParams {},
  hash: ''
} */