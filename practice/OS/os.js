const os = require('os');
const cluster = require('cluster');

// console.log(os.platform()); //win32 идентифицирующую платформу операционной системы
// console.log(os.arch());//x64 архитектуру процессора операционной системы
// console.log(os.cpus().length);//12 ядра логическиу ядра процессора

if (cluster.isMaster) {// проверям на то что текущий процесс - главный
    for (let i = 0; i < os.cpus().length - 2; i++) { 
        cluster.fork()// запускаем на каждом едре по процессу вычитая 2
    }
    cluster.on('exit', (worker, code, signal) => {
        console.log(`Воркер с pid = ${worker.process.pid} умер`) // при смерти процесса выведем код
        if(code === '1') {//emmiter в следующей теме
            cluster.fork()
        } else {
            console.log('Воркер умер...')
        }
    })
} else {
    console.log(`Воркер с pid= ${process.pid} запущен`)

    setInterval(() => {
        console.log(`Воркер с pid= ${process.pid} еще работает`)
    }, 5000)
}