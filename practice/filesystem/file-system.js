const fs = require('fs');
const path = require('path');
console.log('start');

// // fs.mkdirSync(path.resolve(__dirname, 'tes1','tes2','tes3'),{recursive:true});// создаем 3 вложенных директории
// fs.mkdir(path.resolve(__dirname, 'tes1'),(err) => {
//     if(err){
//         console.log(err);
//         return; //цикл
//     }
//     console.log('done')
// })
// console.log('end')

// fs.rmdir(path.resolve(__dirname, 'tes1'),(err) => {
//     if(err){
//         throw(err)
//     }
// })

// fs.writeFile(path.resolve(__dirname, 'test.txt'), 'test', (err) => {
//     if (err) {
//         throw err;
//     }
//     fs.appendFile(path.resolve(__dirname, 'test.txt'), '\ntest2313123313', (err) => {
//         if (err) {
//             throw err;
//         }
//     });
//     fs.appendFile(path.resolve(__dirname, 'test.txt'), '\ntest2313123313', (err) => {
//         if (err) {
//             throw err;
//         }
//     });
// });
/** для создания аннотации
 *
 * @param {*} path
 * @param {*} data
 * @returns
 */



const writeFileAsync = async (path, data) => {
    return new Promise((resolve, reject) => fs.writeFile(path, data,(err) => {
            if (err) {
                return reject(err.message);
            }
            resolve();
        }))
};

const appendFileAsync = async (path, data) => {
    return new Promise((resolve, reject) => fs.appendFile(path,data,(err) => {
        if (err) {
            return reject(err.message);
        }
        resolve();
    }))
};

const readFileAsync = async (path) => {
    
    return new Promise((resolve, reject) => fs.readFile(path,{encoding:'utf-8'},(err,data) => {
        if (err) {
            return reject(err.message);
        }
        resolve(data);
    }))

};
const removeFileAsync = async (path) => {
    return new Promise((resolve, reject) => fs.rm(path,(err) => {
        if (err) {
            return reject(err.message);
        }
        resolve();
    }))
}

writeFileAsync(path.resolve(__dirname, 'test.txt'), 'test')
    .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\ntest2'))
    .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\ntest2'))
    .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\ntest2'))
    .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\ntest2'))
    .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\ntest2'))
    .then(() => readFileAsync(path.resolve(__dirname,'test.txt'))
    .then((data) => console.log(data))
    .catch((err) => console.log(err))
    )
    .then(() => removeFileAsync(path.resolve(__dirname, 'test.txt')))
    .then(console.log('file removed'));

// Через переменную окружения передать строку, записать ее в файл
// прочитать файл, посчитать кол-во слов в файле и записать
// их в новый файл count.txt, затем удалить первый файл

// const text = process.env.TEXT || '';
//
// writeFileAsync(path.resolve(__dirname, 'text.txt'), text) создаем файл 
//     .then(() => readFileAsync(path.resolve(__dirname, 'text.txt')))//читаем файл получаем его содержимое
//     .then(data => data.split(' ').length)// получаем кол-во слов в файле
//     .then(count => writeFileAsync(path.resolve(__dirname, 'count.txt')//Кол-во слов ${count}`))//записываем кол-во слов в файл номер 2
//     .then(() => removeFileAsync(path.resolve(__dirname, 'text.txt')))//удаляем файл



