// Readable - чтение
// Writable - Запись
// Duplex - Для чтения и записи Readable + Writable
// Transform - Такой же как Duplex, но может изменить данные по мере чтения

const fs = require('fs')
const path = require('path');
const { Stream } = require('stream');

/* тут файл будет читаться целиком! */
fs.readFile(path.resolve(__dirname, 'test.txt'), (err, data) => {
    if (err) {
        throw err;
    }
    console.log(data)
})
/* читаем файл по кусочкам! */
const stream = fs.createReadStream(path.resolve(__dirname, 'test2.txt'))

// Один чанк по дефолту 64кб
stream.on('data', (chunk) => {
    console.log(chunk)
})
stream.on('end', () => console.log('Закончили читать'))
stream.on('open', () => console.log('Начали читать'))
stream.on('error', (e) => console.log(e))

const writableStream = fs.createWriteStream(path.resolve(__dirname, 'test2.txt'))
for (let i = 0; i < 20; i++) {
    writableStream.write(i + '\n');// записываем данные в файл от 1 до 20
}

// writableStream.end() 
// writableStream.close() 
// writableStream.destroy()
// writableStream.on('error')

// используем в http запрос, ответ

const http = require('http');
http.createServer((req, res) => {
    //  req - readable stream
    //  res - writable stream

    const stream = fs.createReadStream(path.resolve(__dirname, 'test.txt'))
     // Стрим закончит читать раньше чем пользователь скачает
Stream.on ('data', chunk=> res.write(chunk))
Stream.on('end',chunk=> res.end())
//     stream.pipe(res)// синхронизирует чтение и скачивание!
})