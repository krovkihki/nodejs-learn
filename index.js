const userRouter = require('./src/user-router.js');
const PORT = process.env.PORT || 5000; // set the port из ENV или 5000
const Application = require('./framework/Application');
const app = new Application();
const jsonParse = require('./framework/parseJson');
const parseUrl = require('./framework/parseUrl');
const mongoose = require('mongoose');

app.use(jsonParse);
app.addRouter(userRouter);
app.use(parseUrl('http://localhost:5000'));
const start = async () => {
    try {
        await mongoose.connect(
            'mongodb+srv://krovkihki48:qwerty123@cluster0.38bh6p4.mongodb.net/?retryWrites=true&w=majority'
        );
        app.listen(PORT, () => console.log(`server starterd on PORT ${PORT}`));
    } catch (err) {
        console.log(err); 
    }
};
start();
