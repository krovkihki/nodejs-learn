const user = require('./user-model.js');
const getUser = async (request,respons) => {
    let users;
    if (request.params.id) {
        users = await user.findById(request.params.id);
    } else{
        users = await user.find();
    }
    respons.send(users);
};

const createUser = async (request,respons) => {
    const User =  await user.create(request.body);
    respons.send(user); 
}; 

module.exports = {
    getUser,
    createUser
}