const Router = require('../framework/router');
const router = new Router();
const controller = require('./user-controller');

router.get('/users',controller.getUser);

router.post('/users',controller.createUser);

module.exports = router;
